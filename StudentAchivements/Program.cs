﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;


namespace StudentAchivements
{
    internal class Program
    {
        private static List<Professor> professors = new List<Professor>();
        private const string file = "sa.bin";
        
        public static void Main(string[] args)
        {
            using (Stream stream = File.Open(file, FileMode.OpenOrCreate))
            {
                if (stream.Length != 0)
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    professors = (List<Professor>)binaryFormatter.Deserialize(stream);
                }
            }
            SetupMainMenu();
            using (Stream stream = File.Open(file, FileMode.OpenOrCreate))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, professors);
            }
        }

        private static void SetupMainMenu()
        {
            ConsoleMenu menu = new ConsoleMenu();
            menu.AddMenuItem("Save and exit", () => menu.Exit());
            menu.AddMenuItem("New professor", () =>
            {
                Console.Clear();
                Console.WriteLine("Enter professor name");
                string name = Console.ReadLine();
                Console.Clear();
                Console.WriteLine("Enter professor surename");
                string surename = Console.ReadLine();
                Subject subject = Subject.Biology;
                ConsoleMenu subjMenu = new ConsoleMenu("Choose subject:");
                foreach (var subj in Enum.GetValues(typeof(Subject)))
                {
                    subjMenu.AddMenuItem(Enum.GetName(typeof(Subject), subj), () =>
                    {
                        subject = (Subject) subj;
                        professors.Add(new Professor(name, surename, subject));
                        subjMenu.Exit();
                    });
                }
                subjMenu.Run();
                
            });
            menu.AddMenuItem("Professors", () => {SetupProfessorsMenu();});
            menu.Run();
        }

        private static void SetupProfessorsMenu()
        {
            ConsoleMenu menu = new ConsoleMenu("Show students for professor:");
            foreach (var professor in professors)
            {
                menu.AddMenuItem(professor.GetFullName(), () =>
                {
                    SetupStudentsMenu(professor);
                });
            }
            menu.AddMenuItem("Go back", () => menu.Exit());
            menu.Run();
        }

        private static void SetupStudentsMenu(Professor professor)
        {
            ConsoleMenu menu = new ConsoleMenu("Student menu:");
            menu.AddMenuItem("Go back", () => menu.Exit());
            menu.AddMenuItem("Add new student", () =>
            {
                Console.Clear();
                Console.WriteLine("Enter student name:");
                string name = Console.ReadLine();
                Console.Clear();
                Console.WriteLine("Enter student surename:");
                string surename = Console.ReadLine();
                professor.AddStudent(new Student(name, surename));
                menu.Reset();
            });
            foreach (var student in professor.GetStudents())
            {
                menu.AddMenuItem(student.GetFullName(), () => SetupMarkMenu(student, professor.Subject));
            }
            if(menu.Run())
                SetupStudentsMenu(professor);
        }

        private static void SetupMarkMenu(Student student, Subject subject)
        {
            ConsoleMenu menu = new ConsoleMenu("Mark menu:");
            menu.AddMenuItem("Go back", () => menu.Exit());
            menu.AddMenuItem("Show marks", () =>
            {
                Console.Clear();
                if(student.GetMarksBySubject((subject)).Count != 0)
                {
                    foreach (var mark in student.GetMarksBySubject(subject))
                    {
                        Console.Write(mark);
                        Console.Write(',');
                    }
                    Console.Write('\n');
                }
                else
                {
                    Console.WriteLine("There is no any mark.");
                }
                Console.WriteLine("Press any key for return to previous menu");
                Console.Read();
            });
            menu.AddMenuItem("Add new mark", () =>
            {
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine($"Enter score from 1 to 5 for {Enum.GetName(typeof(Subject), subject)}");
                    int score = Int32.Parse(Console.ReadLine());
                    try
                    {
                        student.AddMark(score, subject);
                        return;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("You entered incorrect score.Please try again!");
                        Thread.Sleep(2500);
                        
                    }
                    
                }
            });      
            menu.Run();
        }
    }
}